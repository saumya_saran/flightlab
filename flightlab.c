#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <dirent.h>
#include <pthread.h>
#include <time.h>
#include <math.h>
#include <lcm/lcm.h>
#include <sys/time.h>

#define EXTERN

#include "bbblib/bbb.h"
#include "delta_arm.h"
#include "flightlab.h"
#include "lcmtypes/pixy_frame_t.h"
#include "lcmtypes/fl_state_t.h"
#include "signal.h"

// Define gyro bias data
// ADJUST THESE FOR YOUR IMU (see read_gyro() code below)
#define GYROX_BIAS  -340
#define GYROY_BIAS  0
#define GYROZ_BIAS  340

int counter = 0;
int counts = 10000;

static const int16_t t[4] = {10, 28, 12, 19};

void printArr_int(double *arr, int num, char name[])
{
	printf("%s: [ ", name);
	for(int k=0; k<num; k++)
	{
		printf(" %lf, ", arr[k]);
	}
	printf("] \n");
}

void printArr(double *arr, int num, char name[])
{
	printf("%s: [ ", name);
	for(int k=0; k<num; k++)
	{
		printf(" %lf, ", arr[k]);
	}
	printf("] \n");
}

void addArr_int(int16_t *arr, int16_t value)
{
	for(int k=1; k<NUM_SAMPLES; k++)
	{
		arr[k-1] = arr[k];
	}
	arr[NUM_SAMPLES-1] = value;
}

//shift out old value and add new value into
void addArr(double *arr, double value)
{
	for(int k=1; k<NUM_SAMPLES; k++)
	{
		arr[k-1] = arr[k];
	}
	arr[NUM_SAMPLES-1] = value;
}

void IMU_init(void *data)
{
  state_t *state = data;
  int i;
  byte buf[10];
  double gyro[3], accel[3], mag[3], temperature;

  // Initialize BBB & I2C ports
  if (bbb_init()) {
    printf("Error initializing BBB.\n");
    return -1;
  }

  state->i2cd_gyro.name = I2C_1;  // I2C2 is enumerated as 1 on the BBB unless I2C1 is enabled
  state->i2cd_gyro.address = 0xD6; // Gyro address (right shifted by 1 to get 7-bit value)
  state->i2cd_gyro.flags = O_RDWR;  

  state->i2cd_accelmag.name = I2C_1;  // Same I2C port as gyro
  state->i2cd_accelmag.address = 0x3A; // Accel/Mag address (right shifted by 1 to get 7-bit value)
  state->i2cd_accelmag.flags = O_RDWR;  
  usleep(1000);

  if (bbb_initI2C(&(state->i2cd_gyro))) { // Open I2C fd for gyro
    printf("Error initializing I2C port %d for gyro.\n", (int) (state->i2cd_gyro.name));
    return -1;
  }
  usleep(1000);
  
  if (bbb_initI2C(&(state->i2cd_accelmag))) { // Open a second fd (same I2C) for accelmag
    printf("Error initializing I2C port %d for accel.\n", (int) (state->i2cd_accelmag.name));
    return -1;
  }
  usleep(1000);

  // Activate accelerometer, magnetometer, and gyro

  buf[0] = 0x20; buf[1] = 0x0F; 
  bbb_writeI2C(&(state->i2cd_gyro), buf, 2);
  usleep(1000);

  // XM control registers: 0x1F - 0x26
  buf[0] = 0x20; buf[1] = 0x67;  // CTRL_REG1_XM:  3=12.5Hz; 7=enable all accel ch.
  bbb_writeI2C(&(state->i2cd_accelmag), buf, 2); 
  usleep(1000);
  buf[0] = 0x24; buf[1] = 0xF0;  // Activate accel, temp through control register 5
  bbb_writeI2C(&(state->i2cd_accelmag), buf, 2); 
  usleep(1000);
  buf[0] = 0x26; buf[1] = 0x00;  // Send 0x00 to control register 7
  bbb_writeI2C(&(state->i2cd_accelmag), buf, 2); 
  usleep(1000);
  
  return;
}

//////////////////////////////////////////
void read_gyro(struct I2C_data *i2cd, double gyro[])
{
  short tempint;
  byte buf, lobyte, hibyte;  // Used to store I2C data
  
  buf = 0x28; // X gyro, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &lobyte, 1);
  usleep(1000);
  buf = 0x29; // X gyro, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &hibyte, 1);
  usleep(1000);
  tempint = (((short) hibyte) << 8) | lobyte;
  // GYROX_BIAS is the zero bias/offset - please adjust to make tempint close to zero for your x-gyro
  // printf("gyro x=%hd\n", tempint+GYROX_BIAS); // With your bias this should be near zero
  gyro[0] = 0.00875*(tempint+GYROX_BIAS);  // 110 is the zero bias/offset - please adjust
  
  buf = 0x2A; // Y gyro, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &lobyte, 1);
  usleep(1000);
  buf = 0x2B; // Y gyro, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &hibyte, 1);
  usleep(1000);
  tempint = (((short) hibyte) << 8) | lobyte;
  // GYROY_BIAS is the zero bias/offset - please adjust to make tempint close to zero for your y-gyro
  // printf("gyro y=%hd\n", tempint + GYROY_BIAS); // With your bias this should be near zero
  gyro[1] = 0.00875*(tempint + GYROY_BIAS);  
  
  buf = 0x2C; // Z gyro, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &lobyte, 1);
  usleep(1000);
  buf = 0x2D; // Z gyro, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &hibyte, 1);
  usleep(1000);
  tempint = ((short) hibyte << 8) | lobyte;
  // GYROZ_BIAS is the zero bias/offset - please adjust to make tempint close to zero for your y-gyro
  // printf("gyro z=%hd\n", tempint + GYROZ_BIAS); // With your bias this should be near zero
  gyro[2] = 0.00875*(tempint + GYROZ_BIAS);

  return;
}


void read_accel(struct I2C_data *i2cd, double accel[])
{
  short tempint;
  byte buf, lobyte, hibyte;  // Used to store I2C data
  
  buf = 0x28; // X accel, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x29; // X accel, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;
  accel[0] = 0.000061*tempint;    // Not sure about negative readings yet???
  
  buf = 0x2A; // Y accel, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x2B; // Y accel, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;
  accel[1] = 0.000061*tempint;
  
  buf = 0x2C; // Z accel, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x2D; // Z accel, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;
  accel[2] = 0.000061*tempint; 

  return;
}


void readIMU(state_t *state, struct I2C_data *i2cd_gyro,struct I2C_data *i2cd_accelmag)
{
    double gyro[3]; double accel[3];
    FILE* rawIMU, *medIMU, *avgIMU;
    struct timeval time;
     rawIMU = fopen("rawIMU.txt","a");
     medIMU = fopen("medIMU.txt","a");
     avgIMU = fopen("avgIMU.txt","a");

     gettimeofday(&(time),NULL);
     double tt = (double)(time.tv_sec)+(double)(time.tv_usec)*1e-6;
     if(rawIMU == NULL || medIMU == NULL || avgIMU == NULL)
     {
          perror("Error opening results file.");
     } 

     else
     {
	    // Read & print I2C data from 9DOF IMU 10 times (1 second delay)
	    read_gyro(i2cd_gyro, gyro); //takes pointer
	//    printf("gyro (deg/sec) = (%.3lf, %.3lf, %.3lf), ", gyro[0], gyro[1], gyro[2]);
	    usleep(1000);

	    read_accel(i2cd_accelmag, accel); //takes pointer
	 //   printf("accel (g) = (%.3lf, %.3lf, %.3lf) \n", accel[0], accel[1], accel[2]);
	    usleep(1000);

	    fprintf(rawIMU,"%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", tt, gyro[0], gyro[1], gyro[2],accel[0], accel[1], accel[2]);

	    //Store raw values
	    addArr(state->acc0_r,accel[0]);
	    addArr(state->acc1_r,accel[1]);
	    addArr(state->acc2_r,accel[2]);
	/*
		printArr(state->acc0_r, NUM_SAMPLES, "Acc Raw X");
		printArr(state->acc1_r, NUM_SAMPLES, "Acc Raw Y");
		printArr(state->acc2_r, NUM_SAMPLES, "Acc Raw Z");
	*/
	    addArr(state->gyro0_r,gyro[0]);
	    addArr(state->gyro1_r,gyro[1]);
	    addArr(state->gyro2_r,gyro[2]);

	/*	printArr(state->gyro0_r, NUM_SAMPLES, "Gyro Raw X");
		printArr(state->gyro1_r, NUM_SAMPLES, "Gyro Raw Y");
		printArr(state->gyro2_r, NUM_SAMPLES, "Gyro Raw Z");
	*/
	    //Median filter
	    addArr(state->acc0_m, median(state->acc0_r, NUM_SAMPLES));
	    addArr(state->acc1_m, median(state->acc1_r, NUM_SAMPLES));
	    addArr(state->acc2_m, median(state->acc2_r, NUM_SAMPLES));

	/*
		printArr(state->acc0_m, NUM_SAMPLES, "Acc Med X");
		printArr(state->acc1_m, NUM_SAMPLES, "Acc Med Y");
		printArr(state->acc2_m, NUM_SAMPLES, "Acc Med Z");
	*/
	    addArr(state->gyro0_m, median(state->gyro0_r, NUM_SAMPLES));
	    addArr(state->gyro1_m, median(state->gyro1_r, NUM_SAMPLES));
	    addArr(state->gyro2_m, median(state->gyro2_r, NUM_SAMPLES));

		gettimeofday(&(time),NULL);
     		tt = (double)(time.tv_sec)+(double)(time.tv_usec)*1e-6;
		fprintf(medIMU,"%lf,%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", tt, state->gyro0_m[4], state->gyro1_m[4], state->gyro2_m[4], state->acc0_m[4], state->acc1_m[4], state->acc2_m[4]);

	/*
		printArr(state->gyro0_m, NUM_SAMPLES, "Gyro Med X");
		printArr(state->gyro1_m, NUM_SAMPLES, "Gyro Med Y");
		printArr(state->gyro2_m, NUM_SAMPLES, "Gyro Med Z");
	*/
	    //Average filter
	    addArr(state->acc0, average(state->acc0_m, NUM_SAMPLES));
	    addArr(state->acc1, average(state->acc1_m, NUM_SAMPLES));
	    addArr(state->acc2, average(state->acc2_m, NUM_SAMPLES));

	/*
		printArr(state->acc0, NUM_SAMPLES, "Acc Avg X");
		printArr(state->acc1, NUM_SAMPLES, "Acc Avg Y");
		printArr(state->acc2, NUM_SAMPLES, "Acc Avg Z");
	*/	

	    addArr(state->gyro0, average(state->gyro0_m, NUM_SAMPLES));
	    addArr(state->gyro1, average(state->gyro1_m, NUM_SAMPLES));
	    addArr(state->gyro2, average(state->gyro2_m, NUM_SAMPLES));
	/*
		printArr(state->gyro0, NUM_SAMPLES, "Gyro Avg X");
		printArr(state->gyro1, NUM_SAMPLES, "Gyro Avg Y");
		printArr(state->gyro2, NUM_SAMPLES, "Gyro Avg Z");
	*/
		gettimeofday(&(time),NULL);
     		tt = (double)(time.tv_sec)+(double)(time.tv_usec)*1e-6;
		fprintf(avgIMU,"%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", tt, state->gyro0[4], state->gyro1[4], state->gyro2[4], state->acc0[4], state->acc1[4], state->acc2[4]);
		fclose(rawIMU);
		fclose(medIMU);
		fclose(avgIMU);
	}
}

void IMU_destroy(struct I2C_data *i2cd_gyro,struct I2C_data *i2cd_accelmag)
{
  bbb_deinitI2C(i2cd_gyro);
  bbb_deinitI2C(i2cd_accelmag);
}



void locationFromTargets(pixy_t *tar1, pixy_t *tar2, double xyz[3], state_t *state)
{
/*
	Note on convention: (0,0) defined as top left in image. Y increases downwards, X increases. 
	Our world frame matches the camera axis definition, with Z pointing downwards! 
		
	Data from Pixy
	0: Color code 10 (octal 12) at (118, 169) size (27, 45) angle 92
	1: Color code 12 (octal 14) at (118, 41) size (26, 42) angle 92
	2: Color code 19 (octal 23) at (192, 42) size (27, 42) angle 92
	3: Color code 28 (octal 34) at (193, 170) size (26, 45) angle 87
		
*/

	//Color code targets: index location refers to target id-1
	int16_t t_oct[4] = {12, 34, 14, 23};
	//int16_t t[4] = {12, 19, 10, 28};

	//Real world coordinates of targets
	double t_world[4][2] = 
	{	
		{-6.0, -10.4},
		{ 6.0,  -10.4},
		{-6.0 , 10.4},
		{ 6.0 , 10.4}
	};

	//Distance between targets in world
	double dist_t12 = 12.0; 
	double dist_t13 = 20.6;
	
	double dist_t34 = dist_t12;
	double dist_t24 = dist_t13;

	double dist_t23 = sqrt(pow(dist_t12,2) + pow(dist_t13,2));
	double dist_t14 = dist_t23;


	double world_dist[4][4] = 
	{
		{0, dist_t12, dist_t13, dist_t14},
		{dist_t12, 0, dist_t23, dist_t24},
		{dist_t13, dist_t23, 0, dist_t34},
		{dist_t14, dist_t24, dist_t34, 0}
	};

	//Target locations in pixels
	int t_pixy[4][2] = 
	{	
		{ 123,  39},
		{ 197,  36},
		{ 128, 168},
		{ 203, 164}	
	};

	double pixy_height = -37.2; 

	double dist_t12_p = sqrt(pow(t_pixy[0][0]-t_pixy[1][0],2) + pow(t_pixy[0][1]-t_pixy[1][1],2)); 
	double dist_t13_p = sqrt(pow(t_pixy[0][0]-t_pixy[2][0],2) + pow(t_pixy[0][1]-t_pixy[2][1],2));
	
	double dist_t34_p = sqrt(pow(t_pixy[2][0]-t_pixy[3][0],2) + pow(t_pixy[2][1]-t_pixy[3][1],2));
	double dist_t24_p = sqrt(pow(t_pixy[1][0]-t_pixy[3][0],2) + pow(t_pixy[1][1]-t_pixy[3][1],2));
	
	double dist_t23_p = sqrt(pow(t_pixy[1][0]-t_pixy[2][0],2) + pow(t_pixy[1][1]-t_pixy[2][1],2));
	double dist_t14_p = sqrt(pow(t_pixy[0][0]-t_pixy[3][0],2) + pow(t_pixy[0][1]-t_pixy[3][1],2));

	double focal = 232.7467;

	double pixel_dist[4][4] = 
	{
		{0, dist_t12_p, dist_t13_p, dist_t14_p},
		{dist_t12_p, 0, dist_t23_p, dist_t24_p},
		{dist_t13_p, dist_t23_p, 0, dist_t34_p},
		{dist_t14_p, dist_t24_p, dist_t34_p, 0}
	};
	

	if(tar1->type == PIXY_T_TYPE_COLOR_CODE && tar2->type == PIXY_T_TYPE_COLOR_CODE)
	{
		//Find out which targets are detected
		int target1, target2; //target id of found target
		for(int i=0; i<4; i++)
		{

			if(tar1->signature == t[i])
			{
				target1 = i;
				//printf("First target detected: %d\n", i);
			}
			if(tar2->signature == t[i])
			{
				target2 = i;
				//printf("Second target detected: %d\n", i);
			}
		}
		
		//printf("Calculating distance\n");
		//distance between targets in pixels
		
		double dist_targets_p = sqrt(pow(state->pixy_avg[target1].x[NUM_SAMPLES-1] - state->pixy_avg[target2].x[NUM_SAMPLES-1], 2) + pow(state->pixy_avg[target1].y[NUM_SAMPLES-1] - state->pixy_avg[target2].y[NUM_SAMPLES-1],2));
		double height = focal * world_dist[target1][target2] / dist_targets_p; 
		//printf("Height: %lf\n", height);

		double x = (state->pixy_avg[target1].x[NUM_SAMPLES-1] - t_pixy[target1][0]) * height / focal;
		double x2 = (state->pixy_avg[target2].x[NUM_SAMPLES-1] - t_pixy[target2][0]) * height / focal;

		double y = (state->pixy_avg[target1].y[NUM_SAMPLES-1] - t_pixy[target1][1]) * height / focal;
		double y2 = (state->pixy_avg[target2].y[NUM_SAMPLES-1] - t_pixy[target2][1]) * height / focal;
		
/*
		double x = 1;
		double x2 = 1.5;
		double y = 1;
		double y2 = 1.5;
		double height = -37;
*/
		//average value
		double x_shift = (x + x2)/2;
		double y_shift = (y + y2)/2; 

		xyz[0] = x_shift;
		xyz[1] = y_shift;
		xyz[2] = height;

		//printf("XYZ inside LFT: X %lf Y %lf Z %lf\n", xyz[0], xyz[1], xyz[2]);
	}
}
/*
int16_t getTdata(int target, int k, state_t * state)
{
	switch(target)
	{
		case 0: return state->t0_data[k];
		case 1: return state->t1_data[k];
		case 2: return state->t2_data[k];
		case 3: return state->t3_data[k];
		default: return -1; //Should never get here
	}
}
*/
bool acceptPixyData(pixy_t *obj, state_t *state)
{
  int16_t x[NUM_SAMPLES];
  int16_t y[NUM_SAMPLES];
  int16_t h[NUM_SAMPLES];
  int16_t w[NUM_SAMPLES];

	bool valid = true;
/*	//find which target it is
	int target = -1;
	for(int j=0; j<4; j++)
	{
		if(obj->signature == t[j])
			target = j;
	}
	
	if(target == -1) //Does not correspond to known target pattern
	{
		printf("Target unknown\n");
		valid = false;
	}
	else
	{
		//Make array out of state values
		for(int k=0; k<NUM_SAMPLES*4; k++)
		{
			int rem = k % 4;
			int ind = floor(k/4);
			switch(rem)
			{
				case 0: x[ind] = getTdata(target, k, state); break;
				case 1: y[ind] = getTdata(target, k, state); break;
				case 2: h[ind] = getTdata(target, k, state); break;
				case 3: w[ind] = getTdata(target, k, state); break;
			}	
		}

		double mean_x = average_int(x, NUM_SAMPLES);
		double st_dev_x = std_int(x, mean_x, NUM_SAMPLES);

		double mean_y = average_int(y, NUM_SAMPLES);
		double st_dev_y = std_int(y, mean_y, NUM_SAMPLES);

		double mean_h = average_int(h, NUM_SAMPLES);
		double st_dev_h = std_int(h, mean_h, NUM_SAMPLES);

		double mean_w = average_int(w, NUM_SAMPLES);
		double st_dev_w = std_int(w, mean_w, NUM_SAMPLES);

		bool zero_std = st_dev_x == 0 || st_dev_y == 0 || st_dev_h == 0 || st_dev_w == 0;
		if(zero_std || state->initialPoints < NUM_SAMPLES ) //generally 0 only if there is no initial data
		{
			valid = true;
			state->initialPoints++;
		}
		else
		{
			valid = (valid && (obj->x > mean_x - st_dev_x) && (obj->x < mean_x + st_dev_x));
			valid = (valid && (obj->y > mean_y - st_dev_y) && (obj->y < mean_y + st_dev_y));
			valid = (valid && (obj->height > mean_h - st_dev_h) && (obj->height < mean_h + st_dev_h));
			valid = (valid && (obj->width > mean_w - st_dev_w) && (obj->width < mean_w + st_dev_w));
		}
		
	}
	
//	}*/
	return valid;
} 

int filterPixy(const pixy_frame_t *msg, pixy_t *objs[], state_t *state)
{
	if(msg->nobjects > 1)
	{
		int numTargets = 0;
		for(int k=0; k<msg->nobjects; k++)
		{
			pixy_t *obj = &msg->objects[k];
			//Checks that CC is a known target pattern
			bool isTarget = obj->signature == t[0] || obj->signature == t[1] || obj->signature == t[2] || obj->signature == t[3];
			if(obj->type == PIXY_T_TYPE_COLOR_CODE && isTarget && acceptPixyData(obj, state))
			{
				objs[numTargets] = obj;
				numTargets++;

				int num = -1;
				for(int j=0; j<4; j++)
				{
					if(obj->signature == t[j])
						num = j;
				}
				if(num != -1){
				addArr_int(state->pixy_raw[num].x,obj->x);
				addArr_int(state->pixy_raw[num].y,obj->y);
				addArr_int(state->pixy_raw[num].height,obj->height);
				addArr_int(state->pixy_raw[num].width,obj->width);

				addArr_int(state->pixy_avg[num].x,average_int(state->pixy_raw[num].x,NUM_SAMPLES));
				addArr_int(state->pixy_avg[num].y,average_int(state->pixy_raw[num].y,NUM_SAMPLES));
				addArr_int(state->pixy_avg[num].height,average_int(state->pixy_raw[num].height,NUM_SAMPLES));
				addArr_int(state->pixy_avg[num].width,average_int(state->pixy_raw[num].width,NUM_SAMPLES));
			}
				/*switch(num)
				{
					case 0: addArr_int(state->pixy_raw[0]->x,obj->x);
						addArr_int(state->pixy_raw[0]->y,obj->y);
						addArr_int(state->pixy_raw[0]->height,obj->height);
						addArr_int(state->pixy_raw[0]->width,obj->width);

						addArr_int(state->t0_data, average())
						break;

					case 1: addArr_int(state->pixy_raw[1]->x,obj->x);
						addArr_int(state->pixy_raw[1]->y,obj->y);
						addArr_int(state->pixy_raw[1]->height,obj->height);
						addArr_int(state->pixy_raw[1]->width,obj->width);
						break;

					case 2: addArr_int(state->pixy_raw[2]->x,obj->x);
						addArr_int(state->t2_data,obj->y);
						addArr_int(state->t2_data,obj->height);
						addArr_int(state->t2_data,obj->width);
						break;

					case 3: addArr_int(state->t3_data,obj->x);
						addArr_int(state->t3_data,obj->y);
						addArr_int(state->t3_data,obj->height);
						addArr_int(state->t3_data,obj->width);
						break;
				}*/

			}
		}
	
		return numTargets;
	}
	else
	{
		//printf("Message contained < 2 objects\n");
		return -1;
	}
	
}


int getLocationFromPixy(const pixy_frame_t *msg, double position[3], state_t *state)
{
	//int16_t t[4] = {12, 19, 10, 28};
	pixy_t *objs[msg->nobjects];

	//Filters out objects that aren't color codes
	int numTargets = filterPixy(msg,objs,state);

	switch(numTargets)
	{
		case -1: //printf("Invalid pixy message received\n"); 
			 break;		
		case 0: //printf("No targets detected\n"); 
			/*printArr_int(state->t0_data, NUM_SAMPLES*4, "T0");
			printArr_int(state->t1_data, NUM_SAMPLES*4, "T1");
			printArr_int(state->t2_data, NUM_SAMPLES*4, "T2");
			printArr_int(state->t3_data, NUM_SAMPLES*4, "T3");
			*/
			break;
		case 1: //printf("Only 1 target detected: %d\n", (objs[0])->signature);
			break;
 		default: ;

			double xyz[3];
			double sumx = 0;
			double sumy = 0; 
			double sumz = 0;
			//printf("Number of Targets: %d\n", numTargets);
			for(int k=0; k<numTargets; k++)
			{
				for(int j=k+1; j<numTargets; j++)
				{
					
					locationFromTargets(objs[k], objs[j], xyz, state);
					sumx+=xyz[0];
					sumy+=xyz[1];
					sumz+=xyz[2];
				}
			}

			position[0] = sumx/combo(numTargets,2);
			position[1] = sumy/combo(numTargets,2);
			position[2] = sumz/combo(numTargets,2);

			//printf("X:%lf, Y:%lf Z:%lf\n", position[0], position[1], position[2]);		
	}

	return numTargets;
}

//Translation from pixy to quad copter base frame
void pixy2quad(double xyz[3])
{
	xyz[1] = xyz[1] + 12.2;
}


//LCM communication for Pixy
void pixy_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const pixy_frame_t *msg, void *userdata)
{
    state_t *state = userdata;
    FILE* pixy, *pixy_avg, *loc;
    struct timeval time;
    //printf("Received message on channel %s, timestamp %" PRId64 "\n",
      //     channel, msg->utime);
           
    // Do some processing of the pixy data here
    // Caution: don't save the msg pointer; the data won't be valid after
    //  this handler function exists
	
	double xyz[3];
	pthread_mutex_lock(&(state->mutex));
	int numTargets = getLocationFromPixy(msg, xyz, state);
	
	pixy = fopen("pixy.txt","a");
	pixy_avg = fopen("pixy_avg.txt","a");
	loc = fopen("location.txt","a");
        if(pixy == NULL || pixy_avg == NULL)
	{
          perror("Error opening results file.");
        } 
	else
	{
	  gettimeofday(&(time),NULL);
     	  double tt = (double)(time.tv_sec)+(double)(time.tv_usec)*1e-6;
	  fprintf(pixy, "%lf\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", tt, 
		state->pixy_raw[0].x[NUM_SAMPLES-1],state->pixy_raw[0].y[NUM_SAMPLES-1],state->pixy_raw[0].height[NUM_SAMPLES-1],state->pixy_raw[0].width[NUM_SAMPLES-1],
		state->pixy_raw[1].x[NUM_SAMPLES-1],state->pixy_raw[1].y[NUM_SAMPLES-1],state->pixy_raw[1].height[NUM_SAMPLES-1],state->pixy_raw[1].width[NUM_SAMPLES-1],
		state->pixy_raw[2].x[NUM_SAMPLES-1],state->pixy_raw[2].y[NUM_SAMPLES-1],state->pixy_raw[2].height[NUM_SAMPLES-1],state->pixy_raw[2].width[NUM_SAMPLES-1],
		state->pixy_raw[3].x[NUM_SAMPLES-1],state->pixy_raw[3].y[NUM_SAMPLES-1],state->pixy_raw[3].height[NUM_SAMPLES-1],state->pixy_raw[3].width[NUM_SAMPLES-1]);

	  fprintf(pixy_avg,"%lf\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", tt, 
		state->pixy_avg[0].x[NUM_SAMPLES-1],state->pixy_avg[0].y[NUM_SAMPLES-1],state->pixy_avg[0].height[NUM_SAMPLES-1],state->pixy_avg[0].width[NUM_SAMPLES-1],
		state->pixy_avg[1].x[NUM_SAMPLES-1],state->pixy_avg[1].y[NUM_SAMPLES-1],state->pixy_avg[1].height[NUM_SAMPLES-1],state->pixy_avg[1].width[NUM_SAMPLES-1],
		state->pixy_avg[2].x[NUM_SAMPLES-1],state->pixy_avg[2].y[NUM_SAMPLES-1],state->pixy_avg[2].height[NUM_SAMPLES-1],state->pixy_avg[2].width[NUM_SAMPLES-1],
		state->pixy_avg[3].x[NUM_SAMPLES-1],state->pixy_avg[3].y[NUM_SAMPLES-1],state->pixy_avg[3].height[NUM_SAMPLES-1],state->pixy_avg[3].width[NUM_SAMPLES-1]);	
	}

	//pixy2quad(xyz);
	
	if(numTargets > 1)
	{

			state->quad_pos[0] = xyz[0];
			state->quad_pos[1] = xyz[1]-12.2;
			state->quad_pos[2] = -xyz[2];
			//printf("Quad Base Position: (%lf, %lf, %lf)\n", state->quad_pos[0], state->quad_pos[1], state->quad_pos[2]);

			if(loc != NULL)
			{
				gettimeofday(&(time),NULL);
     	  			double tts = (double)(time.tv_sec)+(double)(time.tv_usec)*1e-6;
				fprintf(loc,"%lf %lf %lf %lf\n",tts, state->quad_pos[0], state->quad_pos[1], state->quad_pos[2]);
			}

	}

	fclose(pixy);
	fclose(pixy_avg);
	fclose(loc);
	pthread_mutex_unlock(&(state->mutex));
}

void *imu_loop(void *data)
{
	state_t *state = data;
	while(1)
	{
		readIMU(state, &(state->i2cd_gyro), &(state->i2cd_accelmag));
	}
}

void* sensor_loop(void* data){

  state_t* state = data;
  pixy_frame_t_subscribe(state->lcm, "PIXY", pixy_handler, state);

  while(1)
  {
  	lcm_handle(state->lcm);
  }

  return 0;
}


/*
double PWM2deg (double pwm) {}
Coverts input \pwm from servo PWM value to angle in degrees (horizontal is 0).
Input: double PWM
Returns: double angle in degrees
*/
double PWM2deg(double pwm)
{
	//Servo PWM range: 2.5 (straight down) to 12.5 (straight up)
	double min = 2.5; 
	double max = 12.5;
	double range = max-min;
	return ((pwm-2.5)/range)*180 - 90.0;
}

/*
double deg2PWM (double deg) {}
Converts input \deg from degrees to servo PWM.
Input: double degrees
Returns: double PWM
*/
double deg2PWM(double deg)
{
	double min = 2.5;
	double max = 12.5;
	double range = max-min;
	return (deg+90)/180 * range + 2.5;
}

void linear(state_t *state)
{
    printf("Running Trajectory!\n");
    FILE* results;
    struct timeval time;
    double steps[3];
    double step = 50;

    steps[0] = (state->servo_dts[0] - state->servo_dts_old[0]) / step;
    steps[1] = (state->servo_dts[1] - state->servo_dts_old[1]) / step;
    steps[2] = (state->servo_dts[2] - state->servo_dts_old[2]) / step;

    double max = fmax(steps[0], fmax(steps[1], steps[2]));
    double new_servo_dts[3];

    for(int k=0; k<step; k++)
    {
	double new_servo_dts[3];
	for(int i=0; i<3; i++)
	{
		new_servo_dts[i] = state->servo_dts_old[i] + steps[i];
		bbb_setDutyPWM(state->pwm_ch[i], new_servo_dts[i]);
	}
    }
    printf("End Trajectory!\n");
}

void cubic_spline(state_t *state)
{
    // Set PWM Duties
    // allow a max jump of 0.1 PWM at a time
    FILE* results;
    struct timeval time;
    double max_step = 0.05;
    double num_commands = fmax(fabs(state->servo_dts[0]-state->servo_dts_old[0]),
                              fmax(fabs(state->servo_dts[1]-state->servo_dts_old[1]),
                                  fabs(state->servo_dts[2]-state->servo_dts_old[2])))/max_step;
    if(num_commands > 0)
    {
      double new_servo_dts[3];
      for(double j = 0; j < (1+1/num_commands); j += 1/num_commands)
      {
        for(int i = 0; i < 3; i++)
	{
          // Interpolate
          new_servo_dts[i] = state->servo_dts_old[i]
                             + 3*(state->servo_dts[i]-state->servo_dts_old[i])*j*j
                             - 2*(state->servo_dts[i]-state->servo_dts_old[i])*j*j*j;

          bbb_setDutyPWM(state->pwm_ch[i], new_servo_dts[i]);
        }
        // Write new servo positions to file
        gettimeofday(&(time),NULL);
        double tt = (double)(time.tv_sec)+(double)(time.tv_usec)*1e-6;
        results = fopen("results.txt","a");
        if(results == NULL)
	{
          perror("Error opening results file.");
        } 
	else
	{
          double cur_pos[3];
          delta_calcForward(PWM2deg(new_servo_dts[0]),PWM2deg(new_servo_dts[1]),PWM2deg(new_servo_dts[2]),&cur_pos[0],&cur_pos[1],&cur_pos[2]);
          fprintf(results,"%f\t%f\t%f\t%f\t%f\t%f\t%f\n",tt,cur_pos[0],cur_pos[1],cur_pos[2]+29,PWM2deg(new_servo_dts[0]),PWM2deg(new_servo_dts[1]),PWM2deg(new_servo_dts[2]));
        }
        fclose(results);

        pthread_mutex_unlock(&(state->mutex));
        usleep(state->servo_sleep/1.5);
        pthread_mutex_lock(&(state->mutex));
      }
      usleep(state->servo_sleep*100);
      printf("State: %c, angles: %f %f %f\n",state->machine,PWM2deg(state->servo_dts[0]),PWM2deg(state->servo_dts[1]),PWM2deg(state->servo_dts[2]));
    } 
}


/*
Command Loop function
Serves as the hold for the command thread, also where the
influence of the state machine are felt. Depending on the
state, the manipulator acts differently.
*/
void* command_loop(void* data){

  state_t* state = data;
  double angle, new_angles[3];
  struct timeval time;

  // always true while running
  while(1)
  {

  
  // check for instability using IMU
  pthread_mutex_lock(&(state->mutex));

	bool machine = (state->machine == 't' || state->machine == 'f');
	bool z = fabs(1-state->acc2[NUM_SAMPLES-1]) > state->z_acc_threshold;	
	bool xy = sqrt(pow(state->acc0[NUM_SAMPLES-1],2) + pow(state->acc1[NUM_SAMPLES-1],2)) > state->xy_acc_threshold;
  	if((xy || z) && machine) //Pitch/Roll too big
	{
		printf("Z_ACC_THRESHOLD FAILED!\n");
		state->machine = 'c';
	}
  pthread_mutex_unlock(&(state->mutex));

    // guard the state vector
    pthread_mutex_lock(&(state->mutex));

    // state machine switch
    switch(state->machine){

    // state: test
    case 't':
      pthread_mutex_unlock(&(state->mutex));
      usleep(state->servo_sleep);
      pthread_mutex_lock(&(state->mutex));

      double end_pos[3];
      int valid_forward = delta_calcForward(PWM2deg(state->servo_dts[0]),PWM2deg(state->servo_dts[1]),PWM2deg(state->servo_dts[2]),&end_pos[0],&end_pos[1],&end_pos[2]);

      if(valid_forward == -1)
      {
	printf("INVALID Forward Kinematics\n");
      }
      else
      {
	      if(end_pos[2]+29 < 16)
	      {
		printf("End position higher than 0: %lf\n", end_pos[2]);
		printf("Degrees: %lf %lf %lf\n", PWM2deg(state->servo_dts[0]),PWM2deg(state->servo_dts[1]),PWM2deg(state->servo_dts[2]));
		state->servo_dts[0] = deg2PWM(0);
		state->servo_dts[1] = deg2PWM(0);
		state->servo_dts[2] = deg2PWM(0);
	      }
      }
		
      break;

    // state: stow
    case 's':
      angle = deg2PWM(STOW_ANGLE);
      for(int i = 0; i < 3; i++){
        state->servo_dts[i] = angle;
      }
      break;

    // state: center (prior to stowing)
    case 'c':
      angle = deg2PWM(0);
      for(int i = 0; i < 3; i++){
        state->servo_dts[i] = angle;
      }
      state->machine = 's';
      break;

    // state: fetch
    case 'f':
      gettimeofday(&(state->time1),NULL);

	      double ee_q_desired[3] = {-state->quad_pos[0], -state->quad_pos[1], -(CYL_HEIGHT + state->quad_pos[2])};  
	      double angles[3], angles_waypoint[3];
              double minus = 0;

		printf("Quad Z position: %lf, Desired EE: %lf\n", state->quad_pos[2], ee_q_desired[2]);

	      int delta = delta_calcInverse(ee_q_desired[0],ee_q_desired[1],ee_q_desired[2],&angles[0],&angles[1],&angles[2]);

              for(int k = 0; k<15; k++){
                minus += 0.1;
	        delta = delta_calcInverse(ee_q_desired[0],ee_q_desired[1],ee_q_desired[2]-minus,&angles[0],&angles[1],&angles[2]);
		if(delta != -1)
			break;
              }
	      int delta_waypoint = delta_calcInverse(ee_q_desired[0], ee_q_desired[1], ee_q_desired[2]-.5, &angles_waypoint[0],&angles_waypoint[1],&angles_waypoint[2]);

              // correct of inverse kinematics problems (angles > 180 degrees)
              for(int i = 0; i < 3; i++){
                if(angles_waypoint[i] > 90) angles_waypoint[i] -= 360;
                if(angles[i] > 90) angles[i] -= 360;
              }

              // print values
	      printf("Waypoint angles: %lf %lf %lf\n", angles_waypoint[0], angles_waypoint[1], angles_waypoint[2]);
	      printf("Ball angles: %f  %f  %f\n",angles[0],angles[1],angles[2]);

      double end_pos_wpt[3], end_pos_goal[3];
      int valid_wpt = delta_calcForward(angles_waypoint[0],angles_waypoint[1],angles_waypoint[2],&end_pos_wpt[0],&end_pos_wpt[1],&end_pos_wpt[2]);
      int valid_goal = delta_calcForward(angles[0],angles[1],angles[2],&end_pos_goal[0],&end_pos_goal[1],&end_pos_goal[2]);

      if(valid_wpt == -1 || valid_goal == -1)
      {
	printf("INVALID Forward Kinematics\n");
      }
      else if(fabs(end_pos_wpt[0]) < 13.7 && fabs(end_pos_goal[0]) < 13.7)
      {
       // set servo duties for the waypoint position
	if(state->fromWaypoint && delta_waypoint == 0 && !(ee_q_desired[0] == 0 && ee_q_desired[1] == 0 && ee_q_desired[2] == 0)){
		state->servo_dts[0] = deg2PWM(angles_waypoint[0]);
		state->servo_dts[1] = deg2PWM(angles_waypoint[1]);
		state->servo_dts[2] = deg2PWM(angles_waypoint[2]);

	      	state->fromWaypoint = false;
	      }

              // set servo duties of the ball position
	      else if(delta == 0 && !(ee_q_desired[0] == 0 && ee_q_desired[1] == 0 && ee_q_desired[2] == 0)){
		state->servo_dts[0] = deg2PWM(angles[0]);
		state->servo_dts[1] = deg2PWM(angles[1]);
		state->servo_dts[2] = deg2PWM(angles[2]);
	      	state->fromWaypoint = true;
		state->machine = 'c';
	      }
}
      break;

    // state: drop
    case 'd':
      // Check if the quad rotor is centered and low enough
      if(abs(state->quad_pos[0]) < state->t_do &&
         abs(state->quad_pos[1]) < state->t_do &&
         state->quad_pos[2] < state->t_do_z + CYL_HEIGHT){
        // if centered, drop the ping pong ball
        angle = deg2PWM(DROP_ANGLE);
        // switch the state to stow
        state->machine = 's';
      }
      else{
        angle = deg2PWM(STOW_ANGLE);
      }
      for(int i = 0; i < 3; i++){
        state->servo_dts[i] = angle;
      }
      break;

    // state: default (shouldn't occur)
    default:
      printf("Bad state machine assignment, sending to center.\n");
      state->machine = 'c';
      for(int i = 0; i < 3; i++){
        printf("Servo: %d Duty: %f\n",i,state->servo_dts[i]);
      }
    }

    // Collision and boundary check
    if((state->servo_dts[0] > 12.5 || state->servo_dts[0] < 2.5) || (state->servo_dts[1] > 12.5 || state->servo_dts[1] < 2.5) || (state->servo_dts[2] > 12.5 || state->servo_dts[2] < 2.5)){
      printf("Invalid position\n");
      state->machine = 'c';
      state->servo_dts[0] = state->servo_dts_old[0];
      state->servo_dts[1] = state->servo_dts_old[1];
      state->servo_dts[2] = state->servo_dts_old[2];
    }

    // If a valid position is not being commanded, just command the stow position
    double new_pos[3];
    if(delta_calcForward(PWM2deg(state->servo_dts[0]),PWM2deg(state->servo_dts[1]),PWM2deg(state->servo_dts[2]),&new_pos[0],&new_pos[1],&new_pos[2]) == -1){
      printf("INVALID POSITION TRYING TO RETURN TO STOW\n");
      state->machine = 'c';
      for(int i = 0; i < 3; i++){
        state->servo_dts[i] = deg2PWM(STOW_ANGLE);
      }
    }

    // Spline from one servo position to the next!!!
    cubic_spline(state);
    //  linear(state);

    // Update old servo duty holders to current servo duty values
    for(int i = 0; i < 3; i++){
      state->servo_dts_old[i] = state->servo_dts[i];
    }

    // Update previous time holder to current time holder
    state->time2 = state->time1;

    double pos[3];
    // Set state->cur_pos based on servo duty and forward kinematics, if valid
    if(delta_calcForward(PWM2deg(state->servo_dts[0]),PWM2deg(state->servo_dts[1]),
                         PWM2deg(state->servo_dts[2]),&pos[0],&pos[1],&pos[2])){
      state->cur_pos[0] = pos[0];
      state->cur_pos[1] = pos[1];
      state->cur_pos[2] = pos[2]+29; // pixy offset
    }

    // unlock thread
    pthread_mutex_unlock(&(state->mutex));
    // sleep to allow other processes to claim processor
    usleep(state->servo_sleep); // initial guess on a reasonable pause length
  } // end{while}

  return 0;
}

void user_state_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                        const fl_state_t *msg, void *data){
  printf("Recieved: Machine - %c\n",(char)(msg->machine));
}

void* user_loop(void* data){
  state_t* state = data;
  fl_state_t msg;
  pthread_mutex_lock(&(state->mutex));
  msg.machine = (int8_t)(state->machine);
  msg.eex = (int32_t)(state->cur_pos[0]);
  msg.eey = (int32_t)(state->cur_pos[1]);
  msg.eez = (int32_t)(state->cur_pos[2]);
  pthread_mutex_unlock(&(state->mutex));
  const fl_state_t* msg_pt = &msg;
  while(1){
    pthread_mutex_lock(&(state->mutex));
    msg.machine = (int8_t)state->machine;
    fl_state_t_publish(state->lcm,"STATE",msg_pt);
    pthread_mutex_unlock(&(state->mutex));
    usleep(state->servo_sleep);
  }
}

void* recieve_user_loop(void* data){
  state_t* state = data;
  fl_state_t_subscribe(state->lcm,"STATE",user_state_handler,state);
  while(1){
    lcm_handle(state->lcm);
  }
}

/*
Initialize the state vector
*/
state_t* state_init(){
  state_t* state = calloc(1,sizeof(*state));

  state->machine = 's'; // start manipulor in stow position for takeoff

  state->servo_sleep = 1000; // initial guess on a reasonable pause

  state->pwm_ch[0] = 1;
  state->pwm_ch[1] = 2;
  state->pwm_ch[2] = 3;

  state->servo_dts[0] = deg2PWM(STOW_ANGLE);
  state->servo_dts[1] = deg2PWM(STOW_ANGLE);
  state->servo_dts[2] = deg2PWM(STOW_ANGLE);

  state->servo_dts_old[0] = state->servo_dts[0];
  state->servo_dts_old[1] = state->servo_dts[1];
  state->servo_dts_old[2] = state->servo_dts[2];

  state->cur_pos[0] = 0;
  state->cur_pos[1] = 0;
  state->cur_pos[2] = 0;

  state->quad_pos[0] = 0;
  state->quad_pos[1] = 0;
  state->quad_pos[2] = 0;

  state->lcm = lcm_create(NULL);
  
  for(int k=0; k<NUM_SAMPLES; k++)
  {
	//Raw
	state->acc0_r[k] = 0;
  	state->acc1_r[k] = 0;
  	state->acc2_r[k] = 0;

  	state->gyro0_r[k] = 0;
  	state->gyro1_r[k] = 0;
  	state->gyro2_r[k] = 0;

	//Med
	state->acc0_m[k] = 0;
  	state->acc1_m[k] = 0;
  	state->acc2_m[k] = 0;

  	state->gyro0_m[k] = 0;
  	state->gyro1_m[k] = 0;
  	state->gyro2_m[k] = 0;
	

	//Avg
  	state->acc0[k] = 0;
  	state->acc1[k] = 0;
  	state->acc2[k] = 0;

  	state->gyro0[k] = 0;
  	state->gyro1[k] = 0;
  	state->gyro2[k] = 0;
	
  }
    printf("Initializing\n");
    

	state->pixy_raw = malloc(sizeof(struct t_data)*4);
  	state->pixy_med = malloc(sizeof(struct t_data)*4);
	state->pixy_avg = malloc(sizeof(struct t_data)*4);
  
    
    for(int i = 0; i < 4; i++){
  	for(int j=0; j<NUM_SAMPLES; j++)
  	{
  		state->pixy_raw[i].x[j] = (int16_t) 0;
  		state->pixy_raw[i].y[j] = (int16_t) 0;
  		state->pixy_raw[i].height[j] = (int16_t) 0;
  		state->pixy_raw[i].width[j] = (int16_t) 0;
  		state->pixy_med[i].x[j] = (int16_t) 0;
  		state->pixy_med[i].y[j] = (int16_t) 0;
  		state->pixy_med[i].height[j] =(int16_t) 0;
  		state->pixy_med[i].width[j] =(int16_t) 0;
  		state->pixy_avg[i].x[j] = (int16_t)0;
  		state->pixy_avg[i].y[j] = (int16_t)0;
  		state->pixy_avg[i].height[j] = (int16_t) 0;
  		state->pixy_avg[i].width[j] = (int16_t)0;
  	}
  }
      printf("Initialized\n");

  IMU_init(state);

  state->t_do = 2;
  state->t_do_z = 5;
  state->t_pu = 0.5;
  state->t_pu_z = 5;

  state->fromWaypoint = false;
  state->z_acc_threshold = .4;
  state->xy_acc_threshold = .4;

  state->initialPoints = 0;
  return state;
}

/*
*/
void state_destroy(state_t* state){
  if(!state) return;

//  IMU_destroy(&(state->i2cd_gyro),&(state->i2cd_accelmag));
}

/*
*/
int main(int argc, char* argv[])
{

  state_t* state = state_init();

  // Initialize BBB and three PWM channels
  if(bbb_init()){
    printf("Error initializing BBB.\n");
    return -1;
  }

  // initialize all 3 PWM channels
  for(int i = 0; i < 3; i++){
    if(bbb_initPWM(state->pwm_ch[i])){
      printf("Error intializing BBB PWM pin %d.\n", state->pwm_ch[i]);
      // Stop all pwm channels that are already running already running
      for(int j = 0; j < i; j++){
        bbb_setRunStatePWM(state->pwm_ch[j],pwm_stop);
      }
      return -1;
    }
    bbb_setPeriodPWM(state->pwm_ch[i],20000000); // Period is in nanoseconds
    bbb_setDutyPWM(state->pwm_ch[i],state->servo_dts[i]); // Duty cycle percent (0-100)
    bbb_setRunStatePWM(state->pwm_ch[i],pwm_run);
  }

  // Start threads
  pthread_mutex_init(&(state->mutex),NULL);
  pthread_create(&(state->sensor_thread),NULL,sensor_loop,state);
  pthread_create(&(state->imu_thread),NULL,imu_loop,state);
  pthread_create(&(state->command_thread),NULL,command_loop,state);
  pthread_create(&(state->user_thread),NULL,user_loop,state);

  char arg = 'z';
  while(arg != 'q'){
    if(arg == 'g'){
      pthread_mutex_lock(&(state->mutex));
      state->machine = 'c';
      pthread_mutex_unlock(&(state->mutex));
    }
    printf("Enter 'd'=drop, 'c'=center, 't'=test, 'f'=fetch, 'g'=goal, 'a'=angle, 'u'=pick-up thresholds, 'o'=drop-off thresholds, 'q'=quit.\n");
    scanf("%c",&arg);
    if(arg == 'd'){
      printf("State: DROP!\n");
      pthread_mutex_lock(&(state->mutex));
      state->machine = 'd';
      pthread_mutex_unlock(&(state->mutex));
    } else if(arg == 'c'){
      printf("State: CENTER!\n");
      pthread_mutex_lock(&(state->mutex));
      state->machine = 'c';
      pthread_mutex_unlock(&(state->mutex));
    } else if(arg == 't'){
      printf("State: t!\n");
      pthread_mutex_lock(&(state->mutex));
      if(state->machine != 'f' || state->machine != 't'){
        for(int i = 0; i < 3; i++){
          state->servo_dts[i] = deg2PWM(0);
        }
      }
      state->machine = 't';
      pthread_mutex_unlock(&(state->mutex));
    } else if(arg == 'f'){
      printf("State: FETCH!\n");
      pthread_mutex_lock(&(state->mutex));
      if(state->machine != 'f' || state->machine != 't'){
        for(int i = 0; i < 3; i++){
          state->servo_dts[i] = deg2PWM(0);
        }
        cubic_spline(state);
        usleep(state->servo_sleep*20);
      }
      state->machine = 'f';
      pthread_mutex_unlock(&(state->mutex));
    } else if(arg == 'g'){
      if(state->machine != 't'){
        printf("Must be in Test State\n");
        continue;
      }
      double goals[3];
      printf("Enter goal x: ");
      scanf("%lf",&goals[0]);
      printf("Enter goal y: ");
      scanf("%lf",&goals[1]);
      printf("Enter goal z: ");
      scanf("%lf",&goals[2]);
      double angles[3];
      double angles_waypoint[3];
      double angles_waypoint2[3];
 

 
      int delta = delta_calcInverse(goals[0],goals[1],goals[2],&angles[0],&angles[1],&angles[2]);
      int delta_waypoint = delta_calcInverse(goals[0]/3, goals[1]/3, 13.5+((goals[2]-13.5)/3), &angles_waypoint[0],&angles_waypoint[1],&angles_waypoint[2]);

      int delta_waypoint2 = delta_calcInverse(2*goals[0]/3, 2*goals[1]/3, 13.5+(2*(goals[2]-13.5)/3), &angles_waypoint2[0],&angles_waypoint2[1],&angles_waypoint2[2]);
     
      if(delta == 0 && delta_waypoint == 0 && delta_waypoint2 == 0 && !(goals[0] == 0 && goals[1] == 0 && goals[2] == 0))
      {
        pthread_mutex_lock(&(state->mutex));

        state->servo_dts[0] = deg2PWM(angles_waypoint[0]);
        state->servo_dts[1] = deg2PWM(angles_waypoint[1]);
        state->servo_dts[2] = deg2PWM(angles_waypoint[2]);

	pthread_mutex_unlock(&(state->mutex));

	usleep(1000000);

        pthread_mutex_lock(&(state->mutex));

        state->servo_dts[0] = deg2PWM(angles_waypoint2[0]);
        state->servo_dts[1] = deg2PWM(angles_waypoint2[1]);
        state->servo_dts[2] = deg2PWM(angles_waypoint2[2]);

	pthread_mutex_unlock(&(state->mutex));

	usleep(1000000);

	//Set waypoint to be the old angles	
	pthread_mutex_lock(&(state->mutex));
	state->fromWaypoint = true; 
	for(int k=0; k<3; k++)
	{
		state->servo_dts[k] = deg2PWM(angles[k]);
	}
	pthread_mutex_unlock(&(state->mutex));
	usleep(1000000);	

      }
    } else if(arg == 'a'){
      if(state->machine != 't'){
        printf("Must be in Test State\n");
        continue;
      }
      double angles[3];
      printf("Enter servo #1 angle: ");
      scanf("%lf",&angles[0]);
      printf("Enter servo #2 angle: ");
      scanf("%lf",&angles[1]);
      printf("Enter servo #3 angle: ");
      scanf("%lf",&angles[2]);
      double pos[3];
      int delta = delta_calcForward(angles[0],angles[1],angles[2],&pos[0],&pos[1],&pos[2]);
      printf("Forward Kinematics - x: %lf y: %lf z: %lf\n",pos[0],pos[1],pos[2]+29);
      if(delta == 0 && !(pos[0] == 0 && pos[1] == 0 && pos[2] == 0)){
        pthread_mutex_lock(&(state->mutex));
        state->servo_dts[0] = deg2PWM(angles[0]);
        state->servo_dts[1] = deg2PWM(angles[1]);
        state->servo_dts[2] = deg2PWM(angles[2]);
        pthread_mutex_unlock(&(state->mutex));
      }
    } else if(arg == 'u'){
      if(state->machine != 'f'){
        printf("Must be in Fetch State\n");
        continue;
      }
      double t_pu, t_pu_z;
      pthread_mutex_lock(&(state->mutex));
      printf("Current t_pu: %f. Enter new t_pu: ",state->t_pu);
      scanf("%lf",&t_pu);
      printf("Current t_pu_z: %f. Enter new t_pu_z: ",state->t_pu_z);
      scanf("%lf",&t_pu_z);
      state->t_pu = t_pu;
      state->t_pu_z = t_pu_z;
      pthread_mutex_unlock(&(state->mutex));
    } else if(arg == 'o'){
      if(state->machine != 'd'){
        printf("Must be in Drop State\n");
        continue;
      }
      double t_do, t_do_z;
      pthread_mutex_lock(&(state->mutex));
      printf("Current t_do: %f. Enter new t_do: ",state->t_do);
      scanf("%lf",&t_do);
      printf("Current t_do_z: %f. Enter new t_do_z: ",state->t_do_z);
      scanf("%lf",&t_do_z);
      state->t_do = t_do;
      state->t_do_z = t_do_z;
      pthread_mutex_unlock(&(state->mutex));
    } else if(arg == 'q'){
      printf("quiting\n");

      // Put the servos back to stow
      pthread_mutex_lock(&(state->mutex));
      if(state->machine == 'f' || state->machine == 't'){
        state->machine = 'c';
      }
      pthread_mutex_unlock(&(state->mutex));
      usleep(state->servo_sleep*100);

      //pthread_mutex_lock(&(state->mutex));

      // Stop the servos
      for(int j = 0; j < 3; j++){
        bbb_setRunStatePWM(state->pwm_ch[j],pwm_stop);
        printf("Stopping PWM #%i\n",j);
      }

      // End threads
      pthread_join(state->command_thread,NULL);
      printf("Joined command thread.\n");
      pthread_join(state->sensor_thread,NULL);
      printf("Joined sensor thread.\n");
      pthread_join(state->user_thread,NULL);
      pthread_join(state->imu_thread,NULL);

      // Cleanup
      state_destroy(state);
      free(state);

      return 0;
    } else{
      printf("not yet bound to rule\n");
    }

    usleep(state->servo_sleep*10);
  }

  return 0;
}
