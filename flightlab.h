#ifndef __FLIGHTLAB_H__
#define __FLIGHTLAB_H__

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <pthread.h>
#include <dirent.h>
#include <unistd.h>
#include <inttypes.h>
#include <lcm/lcm.h>

#include "lcmtypes/pixy_frame_t.h"

#include "bbblib/bbb.h"
#include "delta_arm.h"


// Hold position prior to firing the ball
#define PRE_RELEASE_X 1.0
#define PRE_RELEASE_Y 1.0
#define PRE_RELEASE_Z 1.0
#define NUM_SAMPLES 5

#define CYL_HEIGHT 17 // cm

#define STOW_ANGLE 30
#define DROP_ANGLE 70

struct t_data{
  int16_t x[NUM_SAMPLES];
  int16_t y[NUM_SAMPLES];
  int16_t height[NUM_SAMPLES];
  int16_t width[NUM_SAMPLES];
};

// State structure to hold any and all values of interest
typedef struct state state_t;
struct state {

  // State Machine Holder: stow, fetch, drop
  char machine;

  // Servo Related Values
  int servo_sleep;
  int pwm_ch[3];
  double servo_dts[3]; // 0-100 percent
  double servo_dts_old[3]; // 0-100 percent

  // Filter Values
  int median_buf;
  int low_pass_buf;

  // Threshold Values
  double t_pu; // pick-up threshold for x,y values
  double t_pu_z; // pick-up threshold for z value
  double t_do; // drop-off threshold for x,y values
  double t_do_z; // drop-off threshold for z value

  // Time Keepers
  struct timeval time1, time2;

  // Quad Position (given by pixy, relative to ground and center of cyl)
  double quad_pos[3];

  // Current Manipulator Position (relative to quad)
  // calculated by delta_calcForward()
  double cur_pos[3];

  // Threads
  pthread_t sensor_thread;
  pthread_t imu_thread;
  pthread_t command_thread;
  pthread_t user_thread;
  pthread_mutex_t mutex;  // For thread guarding

   // LCM
  lcm_t *lcm;

  // IMU raw sensor values
  double acc0_r[NUM_SAMPLES];
  double acc1_r[NUM_SAMPLES];
  double acc2_r[NUM_SAMPLES];
  double gyro0_r[NUM_SAMPLES];
  double gyro1_r[NUM_SAMPLES];
  double gyro2_r[NUM_SAMPLES];

  // IMU sensor values after median filtering
  double acc0_m[NUM_SAMPLES];
  double acc1_m[NUM_SAMPLES];
  double acc2_m[NUM_SAMPLES];
  double gyro0_m[NUM_SAMPLES];
  double gyro1_m[NUM_SAMPLES];
  double gyro2_m[NUM_SAMPLES];

  // IMU sensor values after averaging
  double acc0[NUM_SAMPLES];
  double acc1[NUM_SAMPLES];
  double acc2[NUM_SAMPLES];
  double gyro0[NUM_SAMPLES];
  double gyro1[NUM_SAMPLES];
  double gyro2[NUM_SAMPLES];

  //Pixy data raw
  struct t_data *pixy_raw;
  struct t_data *pixy_med;
  struct t_data *pixy_avg;

  
  int initialPoints;
  bool fromWaypoint;
 
  int pixy_offset;

  double z_acc_threshold;
  double xy_acc_threshold;
 

  // IMU Data
  struct I2C_data i2cd_gyro, i2cd_accelmag;


};

#endif
