# README

# ROB 550 - Section 2 Group 3
# Saumya Saran / Mia Stevens / Jin-Gen Wu

# Compile and Run
The code should be compiled using "make" from the flightlab folder.
To run the code from the flightlab folder, use the commands:
./bin/pixy_driver
./bin/flightlab

# Code Details
User input is through the terminal in which the manipulator code was initialized.
(./bin/flightlab)
Test state can be used to test the forward/inverse kinematics and the range of motion of the manipulator.
Fetch is the autonomous retrieval of the ball.
Drop will release the ball when directly over the cylinder.
Center will return the manipulator safely to the stow postion.
If a commanded position is outside of the manipulator's range of motion or 
is in conflict with one of the manipulator's clamps, the manipulator will not move.

# User Interface
d - sets state to DROP
c - sets state to CENTER, then STOW
t - sets state to TEST, and end effector position to CENTER
f - sets state to FETCH
g - in TEST state, can set end effector position (+z)
a - in TEST state, can set servo angles
o - in DROP state, allows for changes to be made the the drop thresholds
q - quits the program by moving the manipulator to CENTER and then STOW, 
    joining the threads and freeing the state memory
