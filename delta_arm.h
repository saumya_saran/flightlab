#ifndef __DELTA_ARM_H__
#define __DELTA_ARM_H__

#include <stdio.h>
#include <math.h>

// Robot Geometry
#define DELTA_F  15.2//7//6.7 //8.0 // (cm) base reference triangle side length
#define DELTA_E  11.6//7//7.7 //7.0 // (cm) end effector reference triangle side length
#define DELTA_RF  7.5 // top link length
#define DELTA_RE  15.4 //14.5// bottom link length

// Trig Constants
#define PI  3.141592653 // PI
#define SQRT3  1.732051 // sqrt(3)
#define SIN120  0.866025 // sqrt(3)/2
#define COS120  -0.5
#define TAN60  1.732051 // sqrt(3)
#define SIN30  0.5
#define TAN30  0.57735 //1/sqrt(3)

// forward kinematics: (theta1, theta2, theta3) in degrees -> (x0, y0, z0)
// returned status: 0 = ok, -1 = non-existant position
int delta_calcForward(double theta1, double theta2, double theta3, double* x0, double* y0, double* z0);

// inverse kinematics helper function
// calculates angle theta1 (for YZ-plane) (in degrees)
int delta_calcAngleYZ(double x0, double y0, double z0, double* theta);

// inverse kinematics: (x0, y0, z0) -> (theta1, theta2, theta3) in degrees
// returned status: 0 = ok, -1 = non-existant position
int delta_calcInverse(double x0, double y0, double z0, double* theta1, double* theta2, double* theta3);

#endif
