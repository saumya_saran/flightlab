#include <stdio.h>
#include <stdlib.h>
#include "delta_arm.h"
#include <sys/time.h>

double deg2PWM(double deg){
  double min = 2.5;
  double max = 12.5;
  double range = max-min;
  return (deg+90)/180 * range + 2.5;
}

double PWM2deg(double pwm){
  double min = 2.5;
  double max = 12.5;
  double range = max-min;
  return ((pwm-2.5)/range)*180 - 90.0;
}

int main(){

//  struct timeval time,t2;
//  double angle[3],pwm,angle2[3];
//  gettimeofday(&(time),NULL);
  while(1){
//    gettimeofday(&(t2),NULL);
//    printf("time usec: %f\n", (double)(t2.tv_sec-time.tv_sec)+(double)(t2.tv_usec-time.tv_usec)*1e-6);
/*    printf("Angle 1 in degrees: ");
    scanf("%lf",&angle[0]);
    printf("Angle 2 in degrees: ");
    scanf("%lf",&angle[1]);
    printf("Angle 3 in degrees: ");
    scanf("%lf",&angle[2]);
*///    pwm = deg2PWM(angle);
//    printf("PWM: %f\n",pwm);
//    angle2 = PWM2deg(pwm);
  //  printf("New Angle in degrees: %f\n",angle2);
  //  double angles[3],pos[3];
//    delta_calcForward(angle[0],angle[1],angle[2],&pos[0],&pos[1],&pos[2]);
    double pwm[3];
    printf("PWM1: ");
    scanf("%lf",&pwm[0]);
    printf("PWM2: ");
    scanf("%lf",&pwm[1]);
    printf("PWM3: ");
    scanf("%lf",&pwm[2]);
//    printf("Forward: %f %f %f\n",pos[0],pos[1],pos[2]);*/
//    double plus = 4;
//    int check = -1;
//    while(check == -1){
//      printf("Inverse of: %f %f %f\n",pos[0],pos[1],pos[2]+plus);
//      plus = plus - 0.1;
//    }
//    delta_calcInverse(pos[0],pos[1],pos[2],&angles[0],&angles[1],&angles[2]);
//    printf("Inverse: %f %f %f\n",angles[0],angles[1],angles[2]);
//    printf("Forward: %f %f %f\n",pos[0],pos[1],pos[2]);

   double dts_old[3] = {pwm[0]-1,pwm[1]-2,pwm[2]-3};
   double max_step = 0.1;
    double num_commands = fmax(pwm[0]-dts_old[0],fmax(pwm[1]-dts_old[1],pwm[2]-dts_old[2]))/max_step;
    double new_servo_dts[3];
    int i;
    double j;
    for(j = 0; j < (1+1/num_commands); j += 1/num_commands){
      printf("Iter %f: ",j);
      for(i = 0; i < 3; i++){
        new_servo_dts[i] = dts_old[i] + 3*(pwm[i]-dts_old[i])*j*j - 2*(pwm[i]-dts_old[i])*j*j*j;
        printf("%f \t",new_servo_dts[i]);
      }
    printf("\n");
    }
  }

  return 0;
}
